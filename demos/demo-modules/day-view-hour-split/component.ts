import { Component, ChangeDetectionStrategy } from '@angular/core';
import { CalendarEvent } from 'angular-calendar';
import { colors } from '../demo-utils/colors';

@Component({
  selector: 'mwl-demo-component',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'template.html'
})
export class DemoComponent {
  view: string = 'day';

  viewDate: Date = new Date();

  events: CalendarEvent[] = [
    {
      title: 'single',
      color: colors.red,
      start: new Date(new Date().setHours(9, 0, 0, 0))
    },
    {
      title: 'Non editable and deletable event',
      color: colors.red,
      start: new Date(new Date().setHours(10, 0, 0, 0)),
      end: new Date(new Date().setHours(11, 0, 0, 0))
    },
    {
      title: 'Editable event',
      color: colors.yellow,
      start: new Date(new Date().setHours(10, 0, 0, 0)),
      end: new Date(new Date().setHours(13, 0, 0, 0))
    },
    {
      title: 'Deletable event',
      color: colors.blue,
      start: new Date('2017-11-06T18:00:00.000Z'),
      end: new Date('2017-11-06T19:29:59.999Z')
    },
    {
      title: 'x',
      color: colors.red,
      start: new Date(new Date().setHours(11, 20, 0, 0)),
      end: new Date(new Date().setHours(11, 30, 0, 0))
    },
    {
      title: 'test 2',
      color: colors.red,
      start: new Date(new Date().setHours(12, 20, 0, 0)),
      end: new Date(new Date().setHours(13, 15, 0, 0))
    },
    {
      title: 'last test',
      color: colors.red,
      start: new Date(new Date().setHours(13, 0, 0, 0)),
      end: new Date(new Date().setHours(14, 20, 0, 0))
    },
    {
      title: 'y',
      color: colors.red,
      start: new Date(new Date().setHours(12, 0, 0, 0)),
      end: new Date(new Date().setHours(12, 30, 0, 0))
    },
    {
      title: 'a',
      color: colors.red,
      start: new Date(new Date().setHours(15, 0, 0, 0)),
      end: new Date(new Date().setHours(16, 0, 0, 0))
    },
    {
      title: 'b',
      color: colors.red,
      start: new Date(new Date().setHours(15, 30, 0, 0)),
      end: new Date(new Date().setHours(16, 30, 0, 0))
    },
    {
      title: 'c',
      color: colors.red,
      start: new Date(new Date().setHours(17, 30, 0, 0)),
      end: new Date(new Date().setHours(18, 30, 0, 0))
    },
    {
      title: 'x',
      color: colors.red,
      start: new Date(new Date().setHours(1, 0, 0, 0)),
      end: new Date(new Date().setHours(1, 45, 0, 0))
    },
    {
      title: 'y',
      color: colors.red,
      start: new Date(new Date().setHours(1, 15, 0, 0)),
      end: new Date(new Date().setHours(2, 0, 0, 0))
    },
    {
      title: 'z',
      color: colors.red,
      start: new Date(new Date().setHours(1, 30, 0, 0)),
      end: new Date(new Date().setHours(1, 45, 0, 0))
    },
    {
      title: 'adsfadfs',
      color: colors.red,
      start: new Date(new Date().setHours(1, 15, 0, 0)),
      end: new Date(new Date().setHours(1, 30, 0, 0))
    },
    {
      title: 'tester',
      color: colors.red,
      start: new Date(new Date().setHours(1, 45, 0, 0)),
      end: new Date(new Date().setHours(2, 0, 0, 0))
    },
    {
      title: 'aa',
      color: colors.red,
      start: new Date(new Date().setHours(0, 10, 0, 0)),
      end: new Date(new Date().setHours(0, 30, 0, 0))
    },
    {
      title: 'bb',
      color: colors.red,
      start: new Date(new Date().setHours(0, 10, 0, 0)),
      end: new Date(new Date().setHours(0, 30, 0, 0))
    },
    {
      title: 'cc',
      color: colors.red,
      start: new Date(new Date().setHours(0, 10, 0, 0)),
      end: new Date(new Date().setHours(0, 30, 0, 0))
    },
    {
      title: 'dd',
      color: colors.red,
      start: new Date(new Date().setHours(0, 20, 0, 0)),
      end: new Date(new Date().setHours(0, 40, 0, 0))
    },
    {
      title: 'ee',
      color: colors.red,
      start: new Date(new Date().setHours(0, 20, 0, 0)),
      end: new Date(new Date().setHours(0, 40, 0, 0))
    }
  ];
}
