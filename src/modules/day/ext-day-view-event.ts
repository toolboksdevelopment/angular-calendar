import { DayViewEvent } from 'calendar-utils';

export interface DayViewEvent extends DayViewEvent {
  overlappingEventsCount?: number;
}
